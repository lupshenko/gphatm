function DangerousMode()
    set shada="NONE"

    set tabpagemax=1000000

    set nobackup
    set nowritebackup
    set noswapfile
endfunction

function NoMouse()
    set guicursor=
    set mouse=
endfunction

function BoringStatus()
    set number

    set laststatus=2

    let filename = ' %F'
    let metadata = '[%R%M] [%Y] [%{&fenc}] [%{&ff}]'
    let spacer = '%='
    let cursor_position = 'ln %l/%L (%p%%) ch %v '

    let status = [
        \filename,
        \metadata,
        \spacer,
        \cursor_position
    \]

    let &g:statusline = join(status, ' ')
endfunction

function EnablePlugins()
    call plug#begin()

    Plug 'evanleck/vim-svelte'

    call plug#end()
endfunction

function HighlightWhitespace()
    set list

    let nonBreakingSpace = 'nbsp:␣'
    let tabs = 'tab:Тт'
    let spaceIndentation = 'multispace:' . repeat('-', &shiftwidth - 1) . '>'

    let whitespace = [
        \nonBreakingSpace,
        \tabs,
        \spaceIndentation
    \]

    let &l:listchars = join(whitespace, ',')
endfunction

function HighlightShitCode()
    if &l:textwidth == 0
        let &l:textwidth = 80
    endif

    let trailingWhitespace = '\S\s\+$'
    let longLines = '\%>' . &l:textwidth . 'v.\+'

    call matchadd('ErrorMsg', trailingWhitespace)
    call matchadd('ErrorMsg', longLines)
endfunction

function OnNewBuffer()
    call HighlightWhitespace()
    call HighlightShitCode()
endfunction

colorscheme boring

call DangerousMode()
call NoMouse()
call BoringStatus()
call EnablePlugins()

filetype plugin indent off

autocmd VimEnter,BufEnter * call OnNewBuffer()
