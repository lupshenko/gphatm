source $VIMRUNTIME/colors/vim.lua

let colors_name = 'boring'

set notermguicolors

highlight Constant ctermfg=darkblue
highlight Comment ctermfg=darkred

highlight LineNr ctermfg=8
highlight NonText ctermfg=7

highlight StatusLine cterm=none ctermbg=7 ctermfg=0

highlight TabLine cterm=none ctermbg=7 ctermfg=0
highlight TabLineFill ctermfg=7
